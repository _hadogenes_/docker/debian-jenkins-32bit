FROM i386/debian:trixie

ENV PUID=1000 \
    PGID=1000 \
    DEB_REPO="http://ftp.icm.edu.pl/pub/Linux/debian" \
    LANG=en_US.UTF-8

COPY --from=jenkins/inbound-agent /usr/share/jenkins/agent.jar /usr/share/jenkins/
COPY --from=jenkins/inbound-agent /usr/local/bin/jenkins-agent /usr/local/bin/

RUN set -x \
 && export DEBIAN_FRONTEND=noninteractive \
 && rm -f /etc/apt/sources.list /etc/apt/sources.list.d/debian.sources \
 && echo "deb $DEB_REPO trixie main contrib non-free non-free-firmware" >> /etc/apt/sources.list \
 && echo "deb-src $DEB_REPO trixie main contrib non-free non-free-firmware" >> /etc/apt/sources.list \
 && echo "deb http://security.debian.org trixie-security main contrib non-free non-free-firmware" >> /etc/apt/sources.list \
 && echo "deb-src http://security.debian.org trixie-security main contrib non-free non-free-firmware" >> /etc/apt/sources.list \
 && apt-get update \
 && apt-get upgrade -y \
 && apt-get install -y \
      build-essential \
      devscripts \
 && apt-get install --no-install-recommends -y \
      sudo \
      pigz \
      pbzip2 \
      xz-utils \
      zstd \
      git \
      mercurial \
      gpg \
      lsb-release \
      wget \
      parallel \
      gawk \
      locales \
      equivs \
      \
      apt-utils \
      reprepro \
      \
      nfs-common \
      \
      default-jre \
      \
      jq \
      lgogdownloader \
      \
      debsigs \
      \
      play.it \
      convmv \
      icoutils \
      innoextract \
      unrar \
      unzip \
 && locale-gen $LANG \
 && sed -i "s/# \($LANG.*\)/\1/" /etc/locale.gen \
 && echo "LANG=\"$LANG\"" > /etc/default/locale \
 && dpkg-reconfigure locales \
 && update-locale LANG=$LANG \
 && apt-get autoclean \
 && rm -f /etc/apt/apt.conf.d/docker-clean /etc/apt/apt.conf.d/docker-gzip-indexes

RUN groupadd --gid "$PGID" jenkins \
 && useradd --gid "$PGID" --uid "$PUID" -m jenkins \
 && chmod 755 ~jenkins \
 && echo "jenkins ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers \
 && rm -f /var/log/lastlog /var/log/faillog \
 && chmod 666 /etc/passwd

COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]

USER root
